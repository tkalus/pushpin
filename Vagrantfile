# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

# SmartCard Vendor and Product IDs; default to Yubikey NEO 4
SC_V_ID = ENV['SMARTCARD_VEND_ID'] || '0x1050'
SC_P_ID = ENV['SMARTCARD_PROD_ID'] || '0x0116'
PP_USER = ENV['PUSHPIN_SSH_USER'] || ENV['USER']

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "bento/ubuntu-18.04"
  config.vm.provision "shell", privileged: true,  inline: $BIONIC_PROVISION_ROOT
  config.vm.provider :virtualbox do |vb|
    # 1 cpu and 0.5 GB RAM
    vb.cpus = 1
    vb.memory = 512
    # Slam the guest clock to match the host if the guest clock is more than
    # 10 seconds off. This is needed to deal with suspend/resume cycles for laptops.
    vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000]
    # Limit how much cpu time a virtual CPU can use.
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "30"]
    # Force-enable Host Time Sync
    vb.customize ["setextradata", :id, "VBoxInternal/Devices/VMMDev/0/Config/GetHostTimeDisabled", 0]
  end  # config.vm.provider == vb
  config.vm.define "pushpin", primary:true do |pushpin|
    pushpin.ssh.forward_agent = false
    pushpin.vm.hostname = "pushpin"
    pushpin.vm.provision "shell", privileged: true,  inline: $PUSHPIN_PROVISION_ROOT
    pushpin.vm.provision "shell", privileged: false, inline: $PUSHPIN_PROVISION_USER, args: [PP_USER]
    pushpin.vm.provision "shell", privileged: false, inline: $PUSHPIN_EVERYBOOT_USER, run: "always"
    pushpin.vm.provider :virtualbox do |vb|
      # Map in the SmartCard
      vb.customize ['modifyvm', :id, '--usb', 'on']
      vb.customize ['usbfilter', 'add', '0', '--target', :id, '--name', 'SmartCard', '--vendorid', SC_V_ID, '--productid', SC_P_ID]
    end  # pushpin.vm.provider == vb
  end  # config.vm == pushpin
end  # Vagrant.configure

# Privilaged provisioner for artful aardvark
$BIONIC_PROVISION_ROOT = <<BIONIC_PROVISION_ROOT_EOD
#!/bin/bash

set -eux -o pipefail

# Purge unused locales; mask-out `.iso88591`
locale-gen --purge $(localedef --list|egrep "(en_US|en_DK)"|grep -vi iso)

# `DEBIAN_FRONTEND` set to `noninteractive` suppresses the root
# password prompt while installing.
export DEBIAN_FRONTEND=noninteractive

# Update local apt cache
apt update

# Upgrade the box to latest
apt -q -y -o Dpkg::Options::='--force-confnew' --allow-downgrades --allow-remove-essential --allow-change-held-packages dist-upgrade

# Autoremove unneeded stuff
apt -q -y autoremove

BIONIC_PROVISION_ROOT_EOD

# Privilaged provisioner for pushpin
$PUSHPIN_PROVISION_ROOT = <<PUSHPIN_PROVISION_ROOT_EOD
#!/bin/bash

set -eux -o pipefail

apt install -y \
    git \
    gnupg2 \
    gnupg-agent \
    pcscd \
    scdaemon

# Optional/Conveniences
apt install -y \
    bash-completion \
    python3-pip \
    python3-virtualenv \
    vim \
    virtualenv

cat >/usr/local/sbin/pinentry-unattended << EOD
#!/bin/sh
if test x"\\$PINENTRY_USER_DATA" = xinteractive; then
    exec  /usr/bin/pinentry-curses "\\$@"
fi
exit 1
EOD
chmod +x /usr/local/sbin/pinentry-unattended

PUSHPIN_PROVISION_ROOT_EOD

# Unprivelaged provisioner for pushpin
$PUSHPIN_PROVISION_USER = <<PUSHPIN_PROVISION_USER_EOD
#!/bin/bash

set -eux -o pipefail

cat >${HOME}/.bashrc << EOD
source \\${HOME}/.bashrc.local
EOD

cat >${HOME}/.bashrc.local << EOD
ssh-add ()
{
    if [ -n "\\$1" ]; then
        command ssh-add "\\${@}"
    else
        command gpg-connect-agent 'scd restart' /bye > /dev/null
        local ID="\\$(command gpg-connect-agent 'scd serialno' /bye \
          | command awk '/SERIALNO/{print \\$3}' | command head -n 1)"
        if [ -z "\\$ID" ]; then
            echo "No SmartCard"
            return 1
        else
            command gpg-connect-agent 'updatestartuptty' /bye > /dev/null 2>&1
            export PINENTRY_USER_DATA=interactive
            command gpg-connect-agent "scd checkpin \\$ID" /bye \
              | command grep OK > /dev/null 2>&1
            unset PINENTRY_USER_DATA
        fi
    fi
}

gpg-agent()
{
    unset SSH_AGENT_PID
    if ! pgrep gpg-agent > /dev/null; then
        command gpg-agent --daemon > /dev/null
    fi

    if [ "\\${gnupg_SSH_AUTH_SOCK_by:-0}" -ne \\$\\$ ]; then
        export SSH_AUTH_SOCK="\\$(gpgconf --list-dirs agent-ssh-socket)"
    fi
}

gpg-agent
export GPG_TTY="\\$(tty)"
export EDITOR=vim

if echo "\\$-" | grep "i" >/dev/null 2>&1; then
    ssh-add
fi
EOD

mkdir -p ${HOME}/.gnupg
chmod 700 ${HOME}/.gnupg
cat >${HOME}/.gnupg/gpg.conf << EOD
use-agent
personal-cipher-preferences AES256 AES192 AES CAST5
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
cert-digest-algo SHA512
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
EOD

cat >${HOME}/.gnupg/gpg-agent.conf << EOD
pinentry-program /usr/local/sbin/pinentry-unattended
enable-ssh-support
default-cache-ttl 600
max-cache-ttl 7200
EOD

mkdir -p ${HOME}/.ssh
chmod 700 ${HOME}/.ssh
cat >${HOME}/.ssh/config << EOD
Host *
  User $1
EOD

PUSHPIN_PROVISION_USER_EOD

$PUSHPIN_EVERYBOOT_USER = <<PUSHPIN_EVERYBOOT_USER_EOD
#!/bin/bash

set -x -o pipefail
source ${HOME}/.bashrc
gpg-agent

PUSHPIN_EVERYBOOT_USER_EOD
